<!DOCTYPE html>
<html lang="fr">
	<head>
		
	    <meta charset ="UTF-8">
	    <title>Informations Employés</title>
	    <link href="modules/style_accueil_identification.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		
	<?php include __DIR__.'/modules/header.php'; ?>
	    
	    
	    <div class ="paragraph">
			<br><br>
			<h1>Informations Employé</h1>
			
			<h3>Superviseur</h3>
			<p>En tant que superviseur, vous avez tous les droits (modification, rectification, suppression)
À l’issue d’une candidature, vous avez la possibilité de créer un compte dédié à l’employé pour qu’il puisse assurer les enregistrements à l’accueil.
</p>
            <h3>Comment créer un compte employé ?</h3>
            <p>Une fois sur la page d’accueil superviseur, vous aurez 4 liens :
Cliquer sur ajouter un compte, vous pourrez ensuite enregistrer toutes les données relatives à l’employé (nom, prénom, adresse…)
Vous avez la possibilité de choisir son statut (Directeur, agent d’accueil ou technicien)
Ensuite en fonction de son statut, lui accorder soit le droit de lecture ou modification.
Une fois que vous aurez renseigné toutes ces informations et cliqué sur enregistrer, les informations seront stockées automatiquement dans la base de données.
</p>

            <h3>Un employé vient de quitter la morgue ? comment restreindre son droit d’accès sans pour autant supprimer définitivement son compte ?</h3>
            <p>Une option « blacklister » existe sur votre page d’accueil. Cliquez sur ce lien puis entrez les informations concernant la personne a blacklister. 
Une fois ces informations transmises à la base de données. L’employé n’aura plus la possibilité d’accéder à son espace dédié. 
Pourtant toutes les informations le concernant restent stockées.
</p>
			<h3>Comment puis-je consulter des données concernant la morgue de manière générale ?</h3>
			<p>Grâce au bouton consulter sur votre page d’accueil.</p>
			
			
			
		</div>
	    

<footer>
	   <ul id="menu_horizontal">	
		<li><a href="contacter.php"><span class="menu-blanc">Nous contacter-2019-Copyright &#9400;</span></a></li>
		<li><a href="politique_de_confidentialite.php"><span class="menu-blanc">confidentialité</span></a></li>
		<li><a href="mentions_legales.php"><span class="menu-blanc">Mentions légales</span></a></li>
	   </ul>
 </footer> 
	
	</body>
</html>
