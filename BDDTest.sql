-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 09, 2019 at 03:01 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BDDTest`
--

-- --------------------------------------------------------

--
-- Table structure for table `accompagnateur`
--

CREATE TABLE `accompagnateur` (
  `numero_enregistrement` bigint(20) NOT NULL,
  `nom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prenom` varchar(20) CHARACTER SET armscii8 COLLATE armscii8_bin NOT NULL,
  `mail` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `telephone` varchar(30) NOT NULL,
  `lien` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `piece_didentite` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `justificatifs` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `numero_employe_changer` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accompagnateur`
--

INSERT INTO `accompagnateur` (`numero_enregistrement`, `nom`, `prenom`, `mail`, `telephone`, `lien`, `piece_didentite`, `justificatifs`, `numero_employe_changer`) VALUES
(1554814344305, 'fhntyjty', 'htrhtr', 'rhtrh', '50505', '', '', '', 272838),
(1554814785341, 'fhntyjty', 'htrhtr', 'rhtrh', '50505', '', '', '', 272838);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `nom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prenom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mail` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `numero_enregistrement` bigint(20) NOT NULL,
  `mot_de_passe` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`nom`, `prenom`, `mail`, `numero_enregistrement`, `mot_de_passe`) VALUES
('fhntyjty', 'htrhtr', 'rhtrh', 1554814344305, '8IdlKhtklH'),
('fhntyjty', 'htrhtr', 'rhtrh', 1554814785341, 'mHnVBAjobe');

-- --------------------------------------------------------

--
-- Table structure for table `defunt`
--

CREATE TABLE `defunt` (
  `numero_enregistrement` bigint(20) NOT NULL,
  `numero_identite` int(11) NOT NULL,
  `nom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prenom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date_deces` date NOT NULL,
  `date_de_naissance` date NOT NULL,
  `cause_deces` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_sanguin` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nationalite` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `numero_box` int(100) NOT NULL,
  `date_entre` date NOT NULL,
  `date_sortie` date NOT NULL,
  `lieu` varchar(30) DEFAULT NULL,
  `present` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `defunt`
--

INSERT INTO `defunt` (`numero_enregistrement`, `numero_identite`, `nom`, `prenom`, `date_deces`, `date_de_naissance`, `cause_deces`, `group_sanguin`, `nationalite`, `numero_box`, `date_entre`, `date_sortie`, `lieu`, `present`) VALUES
(1554814344305, 4661, 'trhhrhr', 'yyjjtyj', '2019-04-19', '2019-03-01', 'accident de voiture', 'O', 'africain', 2525, '2019-04-04', '2019-04-26', '', ''),
(1554814785341, 4661, 'trhhrhr', 'yyjjtyj', '2019-04-19', '2019-03-01', 'accident de voiture', 'O', 'africain', 2525, '2019-04-04', '2019-04-26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

CREATE TABLE `employe` (
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `identifiant` int(11) NOT NULL,
  `password` varchar(100) NOT NULL,
  `statut` varchar(100) NOT NULL,
  `date_entrer` date NOT NULL,
  `droit_acces` varchar(100) NOT NULL,
  `est_bloque` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paiement`
--

CREATE TABLE `paiement` (
  `n_carte` int(16) NOT NULL,
  `ExpMonth` smallint(6) NOT NULL,
  `ExpYear` smallint(6) NOT NULL,
  `card_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation_date`
--

CREATE TABLE `reservation_date` (
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accompagnateur`
--
ALTER TABLE `accompagnateur`
  ADD PRIMARY KEY (`numero_enregistrement`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`numero_enregistrement`);

--
-- Indexes for table `defunt`
--
ALTER TABLE `defunt`
  ADD PRIMARY KEY (`numero_enregistrement`);

--
-- Indexes for table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`identifiant`,`password`);

--
-- Indexes for table `paiement`
--
ALTER TABLE `paiement`
  ADD PRIMARY KEY (`n_carte`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `defunt`
--
ALTER TABLE `defunt`
  MODIFY `numero_enregistrement` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1554814785342;
--
-- AUTO_INCREMENT for table `employe`
--
ALTER TABLE `employe`
  MODIFY `identifiant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
