<?php
session_start();
include __DIR__.'/modules/dbtrang.php';
if (!isset($_SESSION['uid'])) header('Location: identification.php'); 

if (isset($_POST['nmdp'])) {
	if (trim($_POST['cmdp']) != trim($_POST['nmdp'])) {
		$alert = 'Comfirmation du mot de passe n\'est pas correcte';
	} else {

		$identifiant = $_SESSION['uid'];
		$amdp = trim($_POST['amdp']);
		$nmdp = trim($_POST['nmdp']);

		$sql = "SELECT * FROM employe WHERE identifiant=:identifiant AND password=:password";
		$stmt = $sqlconn->prepare($sql);
		$stmt->bindValue(':identifiant', intval($identifiant));
		$stmt->bindValue(':password', md5($amdp));
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if (sizeof($result) == 0) {
			$alert = "L\'ancien mot de passe n\'est pas correcte";
		} else {
			ModifierMDP($sqlconn, $identifiant, md5($nmdp));
			$alert = "Le mot de passe a été modifié";
		}
	}
}

?>
<!DOCTYPE html>
<html lang=fr>
	<head>
		<meta charset="utf-8">
		
		<title> Page d'enregistrement</title>
		<link href="modules/stl.css" rel="stylesheet" type="text/css">

<?php

if (isset($alert)) {
    echo '<script>alert('.json_encode($alert).');</script>';
}
?>
	</head>
	
	<body>
	<?php include __DIR__.'/modules/header.php'; ?>

<p class="logo">
 <img src="logo2.png" alt="logo" />
</p>
	


<h1>Modification de mot de passe</h1>
<h2>Vous désirez changer de mot de passe ?</h2>


<fieldset>
<form method="post">
		<legend><p><strong>Veuillez remplir tous les champs contenant si dessous :</strong></p></legend> 
		<label for="AncienMotDePasse">Ancien mot de passe*:<label>
		<br>	
		<input type='password' id="ancienMotDePasse" name="amdp" required />
		<br>
		<label for="newMotDePasse">Nouveau Mot de passe*:<label>
		<br>	
		<input type='password' name="nmdp" id="newMotDePasse" required>
		<br>
		<label for="confirmationMotDePasse">Comfirmer nouveau mot de passe*:<label>
		<br>	
		<input type='password' id="comfirmationMotDePasse" name="cmdp" required>
		<br>
		<br>
		<input type="submit" value="Enregistrer">
		
      
</form>
</fieldset> 
		
	

<footer>
<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	</body>	
		
</html >
	
