<!DOCTYPE html>
<html lang="fr">
	<head>
		
	    <meta charset ="UTF-8">
	    <title>Informations Clients</title>
	    <link href="modules/style_accueil_identification.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		
	<?php include __DIR__.'/modules/header.php'; ?>
	    <div class ="paragraph">
	    
	    <p>
			<br><br>
			<h1>Informations clients </h1>
			
			<h3> Obtenir mon identifiant et mot de passe de connexion</h3>
			<p>Lors de votre premier passage à la morgue, après l'enregistrement du défunt, l'agent d'accueil vous donne un ticket de réservation.
Ce ticket est à conserver avec grand précaution, car y figure vos identifiants et mot de passe de connexion. 
Cet identifiant et mot de passe vous permettra de vous connecter a out moment et en toute sécurité.
</p>
			
			<h3>Connexion à votre espace de réservation. </h3>
			<p>Une fois que vous êtes sur la page d’accueil du site. Vous cliquez sur le lien : <a href= "identification">S’identifier</a>. 
Vous serez amené à insérer vos informations de connexion, ceux-ci se trouvent sur le ticket de réservation qui vous a été fourni à l’accueil.
 Attention à bien saisir ces informations.
</p>
			<h3>Réserver une date</h3>
			
			<p>Vous arrivez maintenant sur la page de réservation, ici vous avez la possibilité de choisir la date de début et de fin de votre de réservation. Une fois les dates entrées, en fonction du nombre de jours d’intervalles, vous aurez le prix total à payer et une invitation au paiement.</p>
			
			<h3>Paiement</h3>
			<p>Pour le moment vous avez la possibilité de payer uniquement par paypal.</p>
	        
	        
	    </p>
	    </div>
	    

<footer>
	   <ul id="menu_horizontal">	
		<li><a href="contacter.php"><span class="menu-blanc">Nous contacter-2019-Copyright &#9400;</span></a></li>
		<li><a href="politique_de_confidentialite.php"><span class="menu-blanc">confidentialité</span></a></li>
		<li><a href="mentions_legales.php"><span class="menu-blanc">Mentions légales</span></a></li>
	   </ul>
 </footer> 
	
	</body>
</html>
