<?php

session_start();

if (isset($_GET['deconnexion'])) {
    $_SESSION['uid'] = null;
    header('Location: identification.php'); 
    die;
}

function loadDroitAcces($sqlconn) {
	if (session_status() == PHP_SESSION_NONE) session_start();
	if (!$_SESSION['uid']) return null;
	$sql = "SELECT * FROM employe WHERE identifiant=:identifiant";
	$stmt = $sqlconn->prepare($sql);
	$stmt->bindValue(':identifiant', intval($_SESSION['uid']));
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result[0]['droit_acces'];
}

if (!isset($sqlconn)) include __DIR__.'/modules/dbtrang.php';
$droit_acces = loadDroitAcces($sqlconn);
if ($droit_acces == 'employe') {
	header('Location: page_enregistrement.php');
} else if ($droit_acces == 'tiers') {
	header('Location: statistique.php');
} else if ($droit_acces == 'superviseur') {
	header('Location: accueil_superviseur.php');
} else {
    header('Location: accueil.php');
}

?>