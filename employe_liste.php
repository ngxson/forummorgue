

<?php
       
        include __DIR__.'/modules/dbtrang.php';
        $sth = $sqlconn->prepare('SELECT * FROM employe');
        $sth->execute();
        $donnees = $sth->fetchAll(PDO::FETCH_ASSOC);
	?>
	<!DOCTYPE html>
<html>
<head>
  <title>Liste employés</title>
   <link href="modules/stl.css" rel="stylesheet" type="text/css">
   <meta charset="UTF-8">
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td {
  border: 1px solid #ffffff;
  text-align: left;
  padding: 8px;
}

th {
  background-color: #dddddd;
  text-align: left;
  padding: 8px;
}
</style>
</head>

<body>
<?php include __DIR__.'/modules/header.php'; ?>
<h1>Liste employ&#233;s</h1>
<div class="verte">
	 <fieldset>	
<legend>Liste employ&#233;s	</legend>
    <p>
        <table>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Droit d'acces</th>
                <th>Bloqué</th>
                <th></th>
            </tr>
            <?php foreach($donnees as $employe) {
                $identifiant = htmlspecialchars($employe['identifiant']);
            ?>
            <tr>
                <td><?php echo $identifiant; ?></td>
                <td><?php echo htmlspecialchars($employe['nom']); ?></td>
                <td><?php echo htmlspecialchars($employe['prenom']); ?></td>
                <td><?php echo htmlspecialchars($employe['droit_acces']); ?></td>
                <td><?php echo htmlspecialchars($employe['est_bloque'] ? 'OUI' : 'NON'); ?></td>
                <td>
                    <a href="rechercher_employe_afficher.php?identifiant=<?php echo $identifiant; ?>">Consulter</a> | 
                    <a href="employe_modification.php?identifiant=<?php echo $identifiant; ?>">Modifier</a>
                </td>
            </tr>
            <?php } ?>
        </table>
   </p>
   </fieldset>	

</div>
     <div class="sup">
	 	<footer>
		<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	 </body>
</html>