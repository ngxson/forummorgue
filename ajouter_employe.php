﻿<?php

include __DIR__.'/modules/utils.php';
include __DIR__.'/modules/dbtrang.php';

function processPost($sqlconn) {
	$employe = array(
		'nom' => trim($_POST['nom']),
    	'prenom' => trim($_POST['prenom']),
    	'email' => trim($_POST['email']),
    	'password' => md5(trim($_POST['pass'])),
    	'statut' => trim($_POST['metier']),
    	'date_entrer' => trim($_POST['datedentrer']),
		'droit_acces' => trim($_POST['droitacces']),
		'est_bloque' => 0,
	);
	
	if (strlen(trim($_POST['pass'])) < 4) {
		return 'Le mot de passe est trop court il faut au minimome 6 charactéres';
	}

	if (trim($_POST['pass']) != trim($_POST['cpass'])) {
		return 'La confirmation du mot de passe ne correspond pas au mot de passe';
	}

	$res = dbInsertNew($sqlconn, 'employe', $employe);
	if (isset($res['error'])) return $res['error'];
	else return 'OK';
}

if (isset($_POST['submit'])) {
	$result = processPost($sqlconn);
	$ident_id = $sqlconn->lastInsertId();
}
?>


<!DOCTYPE html>
<html lang=fr>
	<head>
		<meta charset="utf-8">
		
		<title>Ajouter employé</title>
		<link href="modules/stl.css" rel="stylesheet" type="text/css">

		<?php if (isset($result) && $result !== 'OK') {
		    echo '<script>alert('.json_encode($result).');</script>';
		} ?>
	</head>
	
	<body>
	<?php 
	$custom_menu = 'admin';
	include __DIR__.'/modules/header.php';
	?>

<p class="logo">
 <img src="logo2.png" alt="logo" />
</p>
	


<h1>Informations sur l'employé</h1>
<h2>Ajouter employé</h2>


<fieldset>
<form method="post">

<?php if (isset($result) && $result === 'OK') { ?>

	<legend><p><strong>Félicitation votre nouveau employé à été bien enregistrer !! </strong></p></legend> 
	<legend><p>identifiant: <?php echo htmlspecialchars($ident_id); ?></p></legend> 
	<legend><p>Mot de passe: <?php echo htmlspecialchars($_POST['pass']); ?></p></legend> 

<?php } else { ?>

		<legend><p><strong>Veuillez remplir tous les champs contenant si dessous :</strong></p></legend> 

		<label for="nom">Nom*:<label><br>
		<input type='text' id="nom" name="nom" required /><br>
		
		<label for="prenom">Prenom*:<label><br>
		<input type='text' id="prenom" name="prenom" required /><br>
		
		<label for="email">Email*:<label><br>
		<input type='text' id="email" name="email" required /><br>
		
		<label for="pass">Mot de passe*:<label><br>
		<input type='password' id="pass" name="pass" required /><br>

		<label for="cpass">Confirmation du mot de passe*:<label><br>
		<input type='password' id="cpass" name="cpass" required /><br>

		<label for="metier">Métier*:<label><br>
		<input type='text' id="metier" name="metier" required /><br>

		<label for="datedentrer">Date d'entrer*:<label><br>
		<input type='date' id="datedentrer" name="datedentrer" required /><br>

		<label for="droitacces">Droit d'acces:<label><br>
		<select id="droitacces" name="droitacces">
			<option value="employe">Employé</option>
			<option value="tiers">Tiers</option>
			<option value="superviseur">Superviseur</option>
		</select>

		<br><br>
		<input type="submit" name="submit" value="Enregistrer">
		
<?php } ?>
      
</form>
</fieldset> 
		
	

<footer>
<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	</body>	
		
</html >

