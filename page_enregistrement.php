<?php
include __DIR__.'/modules/dbtrang.php';

session_start();
if (!isset($_SESSION['uid'])) header('Location: identification.php'); 
?>
<!DOCTYPE html>
<html lang=fr>
	<head>
		<meta charset="utf-8">
		
		<title> Page d'enregistrement</title>
		<link href="modules/stl.css" rel="stylesheet" type="text/css">
	</head>

	<body>
<?php include __DIR__.'/modules/header.php'; ?>

<p class="logo">
 <img src="logo2.png" alt="logo" />
</p>
<h2>Bienvenue sur votre compte profesionnel</h2>
<p>Veuillez remplir tous les champs contenant le symbole (*) afin d'enregistrer le défunt : </p>
	<div class="verte">
		<fieldset><br>

<?php

if (isset($result) && isset($result['error'])) {
    echo '<script>alert('.json_encode($result['error']).');</script>';
} else if (isset($result) && isset($result['success'])) {
?>
		<legend>Merci</legend>
		<legend>Votre information a été enregistrée</legend>
		<legend>Numéro d'enregistrement: <?php echo $result['numero_enregistrement']; ?></legend>
		<legend>Mot de passe: <?php echo $result['mot_de_passe']; ?></legend>
<?php
} else {

?>
		<legend>Informations :</legend>
		<legend>Saisir les données du défunt</legend>
		<br>
		
		<form method="post" action="page_enregistrement_traiter.php" enctype="multipart/form-data">
		
		<label for="nom">Nom*:<label><input type='text' name="nom" required />
		<br>
		<label for="prenom">Prénom*:<label><input type='text' name="prenom"required>
		<br>
		<label for="numero_box">N°box*:<label><input type='text' name="numero_box"required>
		<br>
		<label for="nationalite">Nationalité*:<label><input type='text' name="nationalite"required>
		<br>
		<label for="numero_identite">N°identite*:<label><input type='text' name="numero_identite"required>
		<br>
		<label for="date_de_naissance">Date de naissance*:</label><input type="date" id="datenaissance" name="date_de_naissance">
   		 <br>
    		<label for="date_deces">Date du décès*:</label> <input type="date" id="datededeces" name="date_deces">
    		<br>
		<label for="cause_deces">Cause du décès*:<label><input type='text' name="cause_deces"required>
		<br>
		<label for="group_sanguin">Groupe sanguin:<label><input type='text' name="group_sanguin">
		<br>
		<label for="date_entre">Date entre*:</label><input type='date' id="dateentre" name="date_entre">
		<br>
		<label for="date_entre">Date sortie*:</label><input type='date' id="datesortie" name="date_sortie">



		<br>
		

		<br>
		<legend>Accompagnateur</legend>
		<br>
		
		<label for="acc_nom">Nom*:<label><input type='text' name="acc_nom"required>
		<br>
		<label for="acc_prenom">Prénom*:<label><input type='text' name="acc_prenom"required>
		<br>
		<label for="acc_mail">Mail:<label><input type='text' name="acc_mail">
		<br>
		<label for="acc_telephone">Telephone:<label><input type="tel" name="acc_telephone">
		<br>
		<label for="date" name="acc_lien">Lien*:</label><br>
		<input type="radio" name="famille" value="famille">famille<br>
		<input type="radio" name="police" value="police">Police<br>
		<input type="radio" name="service" value="service_sante">service sant&#233;<br>
		Importer une pièce d'identit&#233; (JPG):<br>
		<input type="file" name="acc_piece_didentite" ><br>
		Importer un justificatifs (JPG):<br>
		<input type="file" name="acc_justificatifs" ><br>
		<br>
		<legend>Pris en charge par :</legend>
		<br>
		<label for="numero_employe_changer">N°employé:<label><input type='text' name="numero_employe_changer">
		<br>
		<br>
		 <input type="submit" value="Enregistrer">
		 <br>
		 <br>
         <input type="reset" value="Recommencer">
         <br>
		</form >

<?php } ?>

	</fieldset>
</div >
<footer>
<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	</body>	
		
</html >
	
		

