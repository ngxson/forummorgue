<?php

include __DIR__.'/modules/utils.php';
include __DIR__.'/db.php';

$result = array();
$current_time = round(microtime(true) * 1000);

try {
  $defunt = array(
    'numero_enregistrement' => $current_time,
    'nom' => getPostVal('nom', ''),
    'prenom' => getPostVal('prenom', ''),
    'nationalite' => getPostVal('nationalite', ''),
    'numero_identite' => intval(getPostVal('numero_identite', '1')),
    'date_de_naissance' => getPostVal('date_de_naissance', '').' 00:00:00',
    'date_deces' => getPostVal('date_deces', '').' 00:00:00',
    'cause_deces' => getPostVal('cause_deces', ''),
    'group_sanguin' => getPostVal('group_sanguin', ''),
  );
  $result = dbInsertNew($sqlconn, 'defunt', $defunt);
  if (isset($result['error'])) throw new Exception($result['error']);

  $accompagnateur = array(
    'numero_enregistrement' => $current_time,
    'nom' => getPostVal('acc_nom', ''),
    'prenom' => getPostVal('acc_prenom', ''),
    'mail' => getPostVal('acc_mail', ''),
    'telephone' => getPostVal('acc_telephone', ''),
    'lien' => getPostVal('acc_lien', ''),
    'piece_didentite' => getPostFile('acc_piece_didentite'),
    'justificatifs' => getPostFile('acc_justificatifs'),
    'numero_employe_changer' => intval(getPostVal('numero_employe_changer', '1')),
  );
  $result = dbInsertNew($sqlconn, 'accompagnateur', $accompagnateur);
  if (isset($result['error'])) throw new Exception($result['error']);

  $mot_de_passe = generateRandomString(10);
  $client = array(
    'numero_enregistrement' => $current_time,
    'mot_de_passe' => $mot_de_passe,
    'nom' => getPostVal('acc_nom', ''),
    'prenom' => getPostVal('acc_prenom', ''),
    'mail' => getPostVal('acc_mail', ''),
  );
  $result = dbInsertNew($sqlconn, 'client', $client);
  if (isset($result['error'])) throw new Exception($result['error']);

  $result['numero_enregistrement'] = $current_time;
  $result['mot_de_passe'] = $mot_de_passe;

} catch (Exception $e) {
	$result['error'] = $e->getMessage();
}

include __DIR__.'/page_enregistrement.php';

?>
