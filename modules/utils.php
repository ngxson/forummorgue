<?php

function hasPostAttr($name) {
  return isset($_POST[$name]) && $name != '';
}

function getArrayKeys($array) {
  $keys = array();

  foreach ($array as $key => $value) { 
    array_push($keys, $key);
  }

  return $keys;
}

function validateCondition($cond, $err_msg) {
  if (!$cond) {
    throw new Exception($err_msg);
  }
}

function getPostVal($name, $default) {
  if (isset($_POST[$name])) {
    return $_POST[$name];
  } else {
    return $default;
  }
}

function getPostFile($name) {
  if (isset($_FILES[$name]) && $_FILES[$name]['tmp_name'] != '') {
    $path = $_FILES[$name]['tmp_name'];
    $data = file_get_contents($path);
    $base64 = 'data:image/jpeg;base64,' . base64_encode($data);
    return $base64;
  } else {
    return '';
  }
}

function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

?>
