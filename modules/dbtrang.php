<?php

$sql_hostname = "localhost";
$sql_database = "ngxsonco_hientrang";
$sql_username = "root";
$sql_password = "root";
//$sql_username = "ngxsonco_hientra";
//$sql_password = "q!M~tPBY^Ihz";

try {
	//// start of setup mysql connection ////
	$sqlconn = new PDO(
		"mysql:host=$sql_hostname;dbname=$sql_database",
		$sql_username, $sql_password
	);
	$sqlconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//// end of setup mysql connection ////
} catch (PDOException $e) {
	var_dump($e);
}

function dbInsertNew($sqlconn, $table, $obj) {

	// prepair columns name
	$columns = join(',', getArrayKeys($obj));
	$columns_val = ':'.join(',:', getArrayKeys($obj));

	// prepair sql command
	$sql = "INSERT INTO ".$table." (".$columns.") VALUES (".$columns_val.")";
	$stmt = $sqlconn->prepare($sql);

	// bind all params and escape them
	foreach ($obj as $key => $value) { 
		if (is_numeric($value))
			$stmt->bindValue(':'.$key, $value, PDO::PARAM_INT);
		else
			$stmt->bindValue(':'.$key, $value);
	}

	// run the query
	$ret = array();
	try {
		$stmt->execute();
		$ret['success'] = true;
	} catch (PDOException $e) {
		$ret['error'] = $e->getMessage();
	}

	// result
	return $ret;
}


function blockEmployee($sqlconn, $numero, $nom) {
	$sql = "UPDATE employe SET est_bloque=1 WHERE identifiant=:identifiant AND nom=:nom";
	$stmt = $sqlconn->prepare($sql);
	$stmt->bindValue(':identifiant', $numero, PDO::PARAM_INT);
	$stmt->bindValue(':nom', $nom);

	// run the query
	$ret = array();
	try {
		$stmt->execute();
		$ret['success'] = true;
		$ret['rowCount'] = $stmt->rowCount();
	} catch (PDOException $e) {
		$ret['error'] = $e->getMessage();
	}

	// result
	return $ret;
}

function ModifierMDP($sqlconn, $identifiant, $password) {
	$sql = "UPDATE employe SET password=:password WHERE identifiant=:identifiant";
	$stmt = $sqlconn->prepare($sql);
	$stmt->bindValue(':identifiant', $identifiant, PDO::PARAM_INT);
	$stmt->bindValue(':password', $password);

	// run the query
	$ret = array();
	try {
		$stmt->execute();
		$ret['success'] = true;
		$ret['rowCount'] = $stmt->rowCount();
	} catch (PDOException $e) {
		$ret['error'] = $e->getMessage();
	}

	// result
	return $ret;
}

function dbUpdate($sqlconn, $table, $where, $obj) {
	// prepair columns name
	$nwhere = array(); $nobj = array();
	foreach (getArrayKeys($obj) as $key) {
		array_push($nobj, $key."=:".$key);
	}
	foreach (getArrayKeys($where) as $key) {
		array_push($nwhere, $key."=:".$key);
	}

	// prepair sql command
	$sql = "UPDATE ".$table." SET ".join(',', $nobj)." WHERE ".join(' AND ', $nwhere);
	$stmt = $sqlconn->prepare($sql);

	// bind all params and escape them
	foreach ($obj as $key => $value) { 
		if (is_numeric($value))
			$stmt->bindValue(':'.$key, $value, PDO::PARAM_INT);
		else
			$stmt->bindValue(':'.$key, $value);
	}
	foreach ($where as $key => $value) { 
		if (is_numeric($value))
			$stmt->bindValue(':'.$key, $value, PDO::PARAM_INT);
		else
			$stmt->bindValue(':'.$key, $value);
	}

	// run the query
	$ret = array();
	try {
		$stmt->execute();
		$ret['success'] = true;
	} catch (PDOException $e) {
		$ret['error'] = $e->getMessage();
	}

	// result
	return $ret;
}

?>
