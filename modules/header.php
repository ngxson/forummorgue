
<?php

function loadDroitAcces($sqlconn) {
	if (session_status() == PHP_SESSION_NONE) session_start();
	if (!$_SESSION['uid']) return null;
	$sql = "SELECT * FROM employe WHERE identifiant=:identifiant";
	$stmt = $sqlconn->prepare($sql);
	$stmt->bindValue(':identifiant', intval($_SESSION['uid']));
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result[0]['droit_acces'];
}

if (!isset($sqlconn)) include __DIR__.'/dbtrang.php';
$droit_acces = loadDroitAcces($sqlconn);

?>


<header>
	<?php if ($droit_acces == null) { ?>

	<ul id="menu_horizontal">
		<li> <a href="accueil.php" title="Page d'accueil"> <span class="menu-blanc">Accueil </span></a></li>
		<li> <a href="identification.php" title="Page d'identification"> <span class="menu-blanc">S'identifier</span></a> </li>
		<li> <a href="FAQ.php" title="Page FAQ"> <span class="menu-blanc">FAQ</span></a> </li>
		<li> <a href="contacter.php" title="Page contact"> <span class="menu-blanc">Nous contacter</span></a> </li>
	</ul>

	<?php } else if ($droit_acces == 'employe') { ?>

	<ul id="menu_horizontal">
		<li>
			<a href="page_enregistrement.php" title="page d'enregistrement">
				<span class="menu-blanc">Page d'enregistrement</span>
			</a>
		</li>
		<li>
			<a href="consulter_defunt.php" title="Consulter defunt">
				<span class="menu-blanc">Consulter</span>
			</a>
		</li>
		<li>
			<a href="FAQ.php" title="Page FAQ">
				<span class="menu-blanc">FAQ</span>
			</a>
		</li>
		<li>
			<a href="modifier_defunt.php" title="modifier defunt">
				<span class="menu-blanc">Modifier defunt</span>
			</a>
		</li>
	</ul>

	<?php } else if ($droit_acces == 'tiers') { ?>

	<ul id="menu_horizontal">
		<li>
			<a href="consulter_defunt.php" title="Consulter defunt">
				<span class="menu-blanc">Consulter</span>
			</a>
		</li>
		<li>
			<a href="FAQ.php" title="Page FAQ">
				<span class="menu-blanc">FAQ</span>
			</a>
		</li>
		<li>
			<a href="statistique.php" title="Page contact">
				<span class="menu-blanc">Statistique</span>
			</a>
		</li>
	</ul>

	<?php } else if ($droit_acces == 'superviseur') { ?>

	<ul id="menu_horizontal">
		<li>
			<a href="ajouter_employe.php" title="Ajouter employé">
				<span class="menu-blanc">Ajouter employé</span>
			</a>
		</li>
		<li>
			<a href="employe_liste.php" title="Consulter employé">
				<span class="menu-blanc">Liste employés</span>
			</a>
		</li>
		<li>
			<a href="blacklist.php" title="Blacklister">
				<span class="menu-blanc">Blacklister</span>
			</a>
		</li>
		<li>
			<a href="consulter_defunt.php" title="Consulter defunt">
				<span class="menu-blanc">Consulter defunt</span>
			</a>
		</li>
		<li>
			<a href="statistique.php" title="Page contact">
				<span class="menu-blanc">Statistique</span>
			</a>
		</li>
	</ul>

	<?php } ?>

	<div class="deconnexion">
		<select id="menu" onchange="selectMenu()">
			<optgroup>
				<option value="" selected>Mon compte</option>
				<option value="Changer le mot de passe">Changer le mot de passe</option>
				<option value="Déconnexion">Déconnexion</option>
			</optgroup>
			<script>
				function selectMenu() {
					var selected = document.getElementById('menu').value + '';
					document.getElementById('menu').value = '';
					if (selected == 'Changer le mot de passe') {
						window.location.href = "modificationMotDePasse.php";
					} else if (selected == 'Déconnexion') {
						window.location.href = "index.php?deconnexion=true";
					}
				}
			</script>
		</select>
	</div>
</header>
<br>
<br>
<br>