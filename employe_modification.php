<?php

include __DIR__.'/modules/utils.php';
include __DIR__.'/modules/dbtrang.php';

function processPost($sqlconn) {
   $where = array(
		'identifiant' => intval($_POST['identifiant']),
   );
   
	$employe = array(
		'nom' => trim($_POST['nom']),
    	'prenom' => trim($_POST['prenom']),
    	'email' => trim($_POST['email']),
    	'statut' => trim($_POST['metier']),
    	'date_entrer' => trim($_POST['datedentrer']),
	);
	
	/*if (strlen(trim($_POST['pass'])) < 4) {
		return 'Le mot de passe est trop court il faut au minimome 6 charactéres';
	}

	if (trim($_POST['pass']) != trim($_POST['cpass'])) {
		return 'La confirmation du mot de passe ne correspond pas au mot de passe';
	}*/

	$res = dbUpdate($sqlconn, 'employe', $where, $employe);
	if (isset($res['error'])) return $res['error'];
	else return 'OK';
}

if (isset($_POST['submit'])) {
	$result = processPost($sqlconn);
	$ident_id = intval($_POST['identifiant']);
} else {
   $sth = $sqlconn->prepare('SELECT * FROM employe WHERE identifiant = :identifiant');
   $sth->bindValue(':identifiant', intval($_GET['identifiant']));
   $sth->execute();
   $employe = $sth->fetch();
}
?>


<!DOCTYPE html>
<html lang=fr>
	<head>
		<meta charset="utf-8">
		
		<title>Ajouter employé</title>
		<link href="modules/stl.css" rel="stylesheet" type="text/css">

		<?php if (isset($result) && $result !== 'OK') {
		    echo '<script>alert('.json_encode($result).');</script>';
		} ?>
	</head>
	
	<body>
<?php include __DIR__.'/modules/header.php'; ?>

<p class="logo">
 <img src="logo2.png" alt="logo" />
</p>
	


<h1>Modifier l'employé</h1>


<fieldset>
<form method="post">

<?php if (isset($result) && $result === 'OK') { ?>

	<h1>Modification reussi avec succes !</h1>
   La mise a jour a bien été effectuée ! pour l'employé avec  <strong>identifiant :</strong><?php echo htmlspecialchars($ident_id); ?><br />

<?php } else { ?>

      <legend><p><strong>Veuillez remplir tous les champs contenant si dessous :</strong></p></legend> 
      
      <input type='hidden' name="identifiant" value="<?php echo htmlspecialchars($_GET['identifiant']); ?>" />

		<label for="nom">Nom*:<label><br>
		<input type='text' id="nom" name="nom" value="<?php echo htmlspecialchars($employe['nom']); ?>" required /><br>
		
		<label for="prenom">Prenom*:<label><br>
		<input type='text' id="prenom" name="prenom" value="<?php echo htmlspecialchars($employe['prenom']); ?>" required /><br>
		
		<label for="email">Email*:<label><br>
		<input type='text' id="email" name="email" value="<?php echo htmlspecialchars($employe['email']); ?>" required /><br>
		
		<!--label for="pass">Mot de passe*:<label><br>
		<input type='password' id="pass" name="pass" required /><br>

		<label for="cpass">Confirmation du mot de passe*:<label><br>
		<input type='password' id="cpass" name="cpass" required /><br-->

		<label for="metier">Métier*:<label><br>
		<input type='text' id="metier" name="metier" value="<?php echo htmlspecialchars($employe['statut']); ?>" required /><br>

		<label for="datedentrer">Date d'entrer*:<label><br>
      <input type='date' id="datedentrer" name="datedentrer" value="<?php echo htmlspecialchars($employe['date_entrer']); ?>" required /><br>
      
		<br><br>
		<input type="submit" name="submit" value="Enregistrer">
		
<?php } ?>
      
</form>
</fieldset> 
		
	

<footer>
<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	</body>	
		
</html >

