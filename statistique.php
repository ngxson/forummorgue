<?php
session_start();
if (!isset($_SESSION['uid'])) header('Location: identification.php'); 

include __DIR__.'/modules/dbtrang.php';

function getCount($sqlconn, $sql) {
  $stmt = $sqlconn->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return intval($result[0]['nombre']);
}

function getData($sqlconn, $sql) {
  $stmt = $sqlconn->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $result;
}

?>
<!DOCTYPE html>
<html lang=fr>
  <head>
     <meta charset="utf-8">
     <title>Statistique</title>
     <link href="modules/stl.css" rel="stylesheet" type="text/css">
     <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  </head>
  <body>
  <?php include __DIR__.'/modules/header.php'; ?>
<p class="logo">
 <img src="logo2.png" alt="logo" />
</p>
<h1>Bienvenue sur le site gestion de morgue</h1>
   <fieldset>
     <legend><p><strong>Vous cherchez des information sur :</strong></p></legend> 
     <form action="affichage.php" method="post">
        <label for="nb_place">Nombres de places disponibles : <?php 
          echo getCount($sqlconn, "SELECT COUNT(*) AS nombre FROM defunt");
        ?></label><br>
            <br> 
        <!--label for="nb_entrer">Nombres d’entrer ?</label>
            <br>
        <label for="nb_sorties">Nombres de sorties ?</label>
            <br--> 
        <canvas id="chart01"></canvas><br>

        <label for="tranche_age">Informations en fonction de la tranche d'âge ?</label><br>
        <canvas id="chart02"></canvas><br>

        <label for="cause">Informations en fonction de la cause de décès?</label><br>
        <canvas id="chart03"></canvas><br>

        <label for="lieu_de_vie">Informations en fonction de lieu de vie ?</label> <br>
        <canvas id="chart04"></canvas><br>

        <label for="date_naissance">Informations en fonction de la date de naissance ?</label><br>
        <canvas id="chart05"></canvas><br>
        
        <br>
     </form>
   </fieldset>   
  <footer>
  <p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
<footer> 
<script>
var ctx = document.getElementById('chart01').getContext('2d');
var chart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Nombres d’entrée present', 'Nombres de sorties'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: ['rgb(255, 0, 0)', 'rgb(0, 0, 255)'],
            data: [<?php 
              echo getCount($sqlconn, "SELECT COUNT(*) AS nombre FROM defunt WHERE present IS NULL");
            ?>, <?php 
              echo getCount($sqlconn, "SELECT COUNT(*) AS nombre FROM defunt WHERE present IS NOT NULL");
            ?>]
        }]
    },
    options: {
      legend: {
        display: true,
        position: 'right'
      }
    }
});


var ctx = document.getElementById('chart02').getContext('2d');
var chart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Moins de 50 ans', 'Plus de 50 ans'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: ['rgb(255, 0, 0)', 'rgb(0, 0, 255)'],
            data: [<?php 
              echo getCount($sqlconn, "SELECT COUNT(*) AS nombre FROM defunt WHERE date_de_naissance >= '01/01/1960'");
            ?>, <?php 
              echo getCount($sqlconn, "SELECT COUNT(*) AS nombre FROM defunt WHERE date_de_naissance < '01/01/1960'");
            ?>]
        }]
    },
    options: {
      legend: {
        display: true,
        position: 'right'
      }
    }
});

function drawPieChart(data, id, key) {
  var colors = [];
  var labels = [];
  var numbers = [];
  data.forEach(function (d, i) {
    labels.push(d[key]);
    numbers.push(parseInt(d.nombre));
    var hue = 300 * i / data.length;
    colors.push('hsl(' + hue + ',100%,50%)');
  })
  var ctx = document.getElementById(id).getContext('2d');
  var chart = new Chart(ctx, {
      type: 'pie',
      data: {
          labels: labels,
          datasets: [{
              label: '',
              backgroundColor: colors,
              data: numbers
          }]
      },
      options: {
        legend: {
          display: true,
          position: 'right'
        }
      }
  });
}

drawPieChart(
  <?php echo json_encode(getData($sqlconn, "SELECT cause_deces,COUNT(cause_deces) AS nombre FROM defunt GROUP BY cause_deces")); ?>,
  'chart03', 'cause_deces'
);

drawPieChart(
  <?php echo json_encode(getData($sqlconn, "SELECT lieu,COUNT(lieu) AS nombre FROM defunt GROUP BY lieu")); ?>,
  'chart04', 'lieu'
);

drawPieChart(
  <?php echo json_encode(getData($sqlconn, "SELECT YEAR(date_de_naissance) AS annee,COUNT(date_de_naissance) AS nombre FROM defunt GROUP BY YEAR(date_de_naissance)")); ?>,
  'chart05', 'annee'
);
</script>
  </body>
</html>


