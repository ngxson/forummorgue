<?php

// On se connecte à la DB
session_start();
include __DIR__ . '/modules/dbtrang.php';

// On regarde si l'IP de la personne est connus
$sth = $sqlconn->prepare('SELECT * FROM accompagnateur WHERE numero_employe_changer = :identifiant');
$sth->bindValue(':identifiant', intval($_SESSION['uid']));
$sth->execute();
$erreur = '';
$accompagnateur = $sth->fetch();
if (!$accompagnateur) $erreur = 'Aucun defunt est de ce compte';

$sth2 = $sqlconn->prepare('SELECT * FROM defunt WHERE numero_enregistrement='.$accompagnateur['numero_enregistrement']);
$sth2->execute();
$defunt = $sth2->fetch();
if (!$defunt) $erreur = 'Aucun defunt est de ce compte';
?>
	<!DOCTYPE html>
<html>
<head>
  <title>Informations sur l'employé</title>
   <link href="modules/stl.css" rel="stylesheet" type="text/css">
   <meta charset="UTF-8">
</head>

<body>
<?php include __DIR__.'/modules/header.php'; ?>

<h1>Bienvenue sur le site gestion de morgue</h1>
<div class="verte">
<fieldset>	

<?php if ($erreur == '') { ?>
  <legend>Informations sur le défunt</legend>
  <p>
  <strong>N° d'enregistrement :</strong> <?php echo  $defunt['numero_enregistrement']; ?><br />
  <strong>N° d'identité :</strong> <?php echo  $defunt['numero_identite'];?><br />
  <strong>Nom :</strong> <?php echo  $defunt['nom']; ?><br />
  <strong>Prénom :</strong> <?php echo  $defunt['prenom'];?><br />
  <strong>Date de décès :</strong> <?php echo  $defunt['date_deces'];?><br />
  <strong>Date de naissance :</strong> <?php echo $defunt['date_de_naissance']; ?><br />
  <strong>Cause de décès :</strong> <?php echo $defunt['cause_deces']; ?><br />
  <strong>Groupe sanguin :</strong> <?php echo $defunt['group_sanguin']; ?><br />
  <strong>Nationalité :</strong> <?php echo $defunt['nationalite']; ?><br />
  <strong>N° box :</strong> <?php echo  $defunt['numero_box']; ?><br />
  <strong>Date d'entrer :</strong> <?php echo $defunt['date_entre']; ?><br />
  <strong>Date de sortie :</strong> <?php echo $defunt['date_sortie']; ?><br />
  <strong>Lieu :</strong> <?php echo $defunt['lieu']; ?><br />
  <strong>Present :</strong> <?php echo $defunt['present']; ?><br /> 
  </p>
<?php } else { ?>
  <p><strong><?php echo $erreur; ?></strong></p>
<?php } ?>

</fieldset>	

</div>
     <div class="sup">
	 	<footer>
		<p><a href="contacter.php" id="contacter">Nous contacter-2019-Copyright &#9400;</a></p>
 <footer>
	 </body>
</html>
         
